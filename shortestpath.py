routes = {
    'a' : {'b':12, 'c':4, 'd':7},
    'b' : {'c':8,'f':5},
    'c' : {'f':6, 'd':1},
    'd' : {'e':5, 'g':6},
    'e' : {'g':3, 'h':4},
    'f' : {'e':1, 'h':8},
    'g' : {'h':3,'b':10},
    'h' : {'g':15,'e':1}
}




def disktra(routes,start,goal):
    unNodes = routes
    shortdist= {}
    infin = 999999
    path = []
    trackpred ={}
   
    
    for node in unNodes:
        shortdist[node] = infin
    shortdist[start] = 0
    
    while unNodes:
        min_node = None
        
        for node in unNodes:
            if min_node is None:
                min_node = node
            elif shortdist[node] < shortdist[min_node]:
                min_node = node
        
        path_options = routes[min_node].items()
        
        for chnode, weight in path_options:
            if weight + shortdist[min_node] < shortdist[chnode]:
                shortdist[chnode] = weight + shortdist[min_node]
                trackpred[chnode] = min_node
                
        unNodes.pop(min_node)
    
    curnode = goal
    
    while curnode != start:
        try:
            path.insert(0,curnode)
            curnode = trackpred[curnode]
        except KeyError:
            print("Path is not reachable")
            break
    
    
    path.insert(0,start)
    
    if shortdist[goal] != infin:
        print("fastest route : " + str(path))
        print("short distance : " + str(shortdist[goal]))
     
    





disktra(routes,'b','g') 